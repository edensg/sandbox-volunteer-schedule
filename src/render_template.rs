use std::error::Error;
use std::fs::File;
use std::io::prelude::*;

use handlebars::Handlebars;

use Config;

mod records {
  use std::fmt;
  use std::slice::Iter;

  #[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
  pub enum Weekday {
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday,
  }

  impl Weekday {
    pub fn iter() -> Iter<'static, Weekday> {
      use self::Weekday::*;
      static DAYS: [Weekday; 7] = [
        Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday,
      ];
      DAYS.into_iter()
    }
  }

  impl fmt::Display for Weekday {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
      write!(f, "{:?}", self)
    }
  }

  #[derive(Serialize, Deserialize, Debug)]
  pub struct Record {
    pub name: String,
    pub student_number: String,
    pub day: Weekday,
    pub start_hour: u8,
    pub end_hour: u8,
    pub project: Option<String>,
  }

  #[derive(Serialize, Deserialize, Debug)]
  pub struct Records {
    pub records: Vec<Record>,
  }

}

mod schedule {
  use render_template::records::Weekday;

  #[derive(Serialize, Deserialize, Debug, Clone)]
  pub struct Person {
    pub name: String,
    pub project: Option<String>,
    pub color: String
  }

  #[derive(Serialize, Deserialize, Debug, Clone)]
  pub struct Day {
    pub day: Weekday,
    pub people: Vec<Person>,
  }

  #[derive(Serialize, Deserialize, Debug, Clone)]
  pub struct Hour {
    pub hour: u8,
    pub days: Vec<Day>,
  }

  #[derive(Serialize, Deserialize, Debug, Clone)]
  pub struct Schedule {
    pub hours: Vec<Hour>,
  }
}

use self::records::*;
use self::schedule::*;

fn read_records<R: Read>(reader: R) -> Result<Records, Box<Error>> {
  let mut rdr = csv::Reader::from_reader(reader);
  let mut records = Records {
    records: Vec::new(),
  };
  for result in rdr.deserialize() {
    // Notice that we need to provide a type hint for automatic
    // deserialization.
    let record: Record = result?;
    records.records.push(record);
  }
  Ok(records)
}

fn process_data(records: Records) -> Schedule {
  let mut schedule = Schedule {
    hours: {
      let mut hours = Vec::new();
      for hour in 10..=18 {
        hours.push(Hour {
          hour: hour,
          days: {
            let mut days = Vec::new();
            for day in Weekday::iter() {
              days.push(Day {
                day: day.clone(),
                people: Vec::new(),
              });
            }
            days
          },
        });
      }
      hours
    },
  };

  for (index, mut record) in records.records.iter().enumerate() {
    for mut hour in schedule.hours.iter_mut() {
      if hour.hour >= record.start_hour && hour.hour < record.end_hour {
        for mut day in hour.days.iter_mut() {
          if record.day == day.day {
            println!("Adding {:?} to {:?} at {:?}", &record, &day.day, &hour.hour);
            day.people.push(Person {
              name: record.name.clone(),
              project: record.project.clone(),
              color: hsluv::hsluv_to_hex(((index as f64) / (records.records.len() as f64) * 360.0, 80.0, 50.0))
            });
          }
        }
      }
    }
  }

  // for mut record in records.records.iter() {
  //   for mut day in schedule.days.iter_mut() {
  //     if record.day == day.day {
  //       for mut hour in day.hours.iter_mut() {
  //         if hour.hour >= record.start_hour && hour.hour <= record.end_hour {
  //           println!("Adding {:?} to {:?} at {:?}", &record, &day.day, &hour.hour);
  //           hour.names = if hour.names.len() > 0 {
  //             format!("{}, {}", hour.names, record.name)
  //           } else {
  //             record.name.clone()
  //           }
  //         }
  //       }
  //     }
  //   }
  // }
  let schedule_copy = schedule.clone();
  println!(
    "{}",
    &schedule_copy
      .hours
      .iter()
      .map(|day| day
        .days
        .iter()
        .filter_map(|hour| if hour.people.is_empty() {
          None
        } else {
          Some(hour)
        })
        .collect::<Vec<_>>())
      .map(|hour| format!("{:?}\n", hour))
      .fold(String::new(), |acc, v| format!("{}{}", acc, v))
  );

  schedule
}

pub fn render_template(config: &Config) -> String {
  let records = read_records(File::open(&config.data_filename).expect("Error opening data file"))
    .expect("error reading records from file");
  let schedule = process_data(records);
  let mut f = File::open(&config.template_filename).expect("error loading template");
  let mut template = String::new();
  f.read_to_string(&mut template)
    .expect("error reading template");
  let reg = Handlebars::new();
  reg
    .render_template(&template, &schedule)
    .unwrap_or(String::from("error rendering handlebars template"))
}
